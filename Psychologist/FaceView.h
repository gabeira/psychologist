//
//  FaceView.h
//  Happiness
//
//  Created by Gabriel Pereira on 8/22/12.
//  Copyright (c) 2012 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FaceView;

@protocol FaceViewDataSource

- (float)smileForFaceView:(FaceView *)sender;

@end

@interface FaceView : UIView

@property (nonatomic) CGFloat scale;

- (void)pinch:(UIPinchGestureRecognizer *)gesture;

@property (nonatomic,weak) IBOutlet id <FaceViewDataSource> dataSource;
@end
