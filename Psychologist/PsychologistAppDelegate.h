//
//  PsychologistAppDelegate.h
//  Psychologist
//
//  Created by Gabriel Pereira on 8/22/12.
//  Copyright (c) 2012 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PsychologistAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
