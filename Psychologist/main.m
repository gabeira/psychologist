//
//  main.m
//  Psychologist
//
//  Created by Gabriel Pereira on 8/22/12.
//  Copyright (c) 2012 Gabriel Bernardo Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PsychologistAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PsychologistAppDelegate class]));
    }
}
